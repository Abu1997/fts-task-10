import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  readonly APIUrl = "http://localhost:60486/api";
  readonly PhotoUrl = "http://localhost:60486/Photos";

  constructor( private http:HttpClient) { }

  getDepList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl +'/Departments');
  }


  addDepartment(val:any){
    return this.http.post<any>(this.APIUrl+'/Departments',val);
  }

  updateDepartment(val:any){
    return this.http.put<any>(this.APIUrl+'/Departments',val);
  }

  deleteDepartment(val:any){
    return this.http.delete<any>(this.APIUrl+'/Departments/'+val);
  }


  getEmplist():Observable<any[]>{
    return this.http.get<any>(this.APIUrl +'/Employee');
  }


  addEmployee(val:any){
    return this.http.post<any>(this.APIUrl+'/Employee',val);
  }

  updateEmployee(val:any){
    return this.http.put<any>(this.APIUrl+'/Employee',val);
  }

  deleteEmployee(val:any){
    return this.http.delete<any>(this.APIUrl+'/Employee/'+val);
  }

  uploadphoto(val:any){
    return this.http.post<any>(this.APIUrl+'/Employee/SaveFile',val);
  }
  getAllDepartmentNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/Employee/GetAllDepartmentNames');
  }
}
